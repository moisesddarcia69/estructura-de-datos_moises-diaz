#include <stdio.h>
#include <string.h>

//Sirve para dar una estructura de los datos que se deben almacenar
struct estudiante{
    int carnet;
    char nombre;
}arreglo_estudiantes[10];

//Le asigno la estructura a la variable estudiantes
typedef struct estudiante estudiante_aux;


int main(){
    estudiante_aux estudiante;
    int x
    int carnet_digitado;
    //Con este ciclo le pregunto al usuario x cantidad de veces el nombre y el carnet
    for(int x=0;x<9;x+=1){
        printf("Ingrese el nombre:");
        scanf("%s",&arreglo_estudiantes[x].nombre);
        printf("Digite el carnet:");
        scanf("%d",&arreglo_estudiantes[x].carnet);
    }
    //Con este verifico el carnet
    printf("Que posicion de carnet desea validar?\n");
    scanf("%d", &x);
    printf("Cual es el carnet del estudiante en la posicion %d?\n",x);
    scanf("%d",&carnet_digitado);
    if(carnet_digitado == arreglo_estudiantes[x].carnet){
        printf("El carnet ingresado es correcto\n");
    }
    else{
        printf("El carnet ingresado no es correcto\n");
    }
    return 0;
}